//9140  Abdul Bari

#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

void FileRedirection(int argc, char* argv[])
{
    /*
     * 
     * This function is redirecting the arguments received by user from index > 1
     * creating a textfile with all the accesses (00700)
     * creating a copy of fD with stdOut
     * 
     */
    int fileDescVal = creat("./textfile.txt",00700);
    //creating file and saving it as an output field.
    
    //saving the fD value to again redirect it to its original fD
    int temp = dup(1);                      
    
    //forcing to write all data 
    fflush(stdout); 
    
    dup2(fileDescVal,STDOUT_FILENO);
    //writing to fD
    
    //writing arguments to fD which is in our case, is the textfile
    for(int i = 0;i<argc;i++)
    {
        printf("%8s\t",argv[i]);
    }
    
    fflush(stdout); 
                                    
    //redirect through the temp val we saved above for redirecting back to original
    dup2(temp,STDOUT_FILENO);               
    
    
    
}

 
void WordCount()
{/*
 * this function is counting the words from ls and showing its result 
 * like ls | wc command does
 */
    
    
    //passing array of 2 elements to pipe function for creating a pipe... (pipe has 2 sides.. thats why 2 elements)
    //it will be use for interprocess communication
    
    int pipe_fd[2];
    pipe(pipe_fd);
    
    //creating a child process and saving its ID (returned) in pid
    pid_t pid = fork();
    
    if(pid == 0)
    {
        close(pipe_fd[0]);
        dup2(pipe_fd[1] , 1);        
        execlp("ls", "ls",NULL);
        perror("Error : Argument\n");
        exit(-1);
    }

    close(pipe_fd[1]);
    
    wait(NULL);
    
    dup2(pipe_fd[0],0);
    
    execlp("wc","wc",NULL);
    
}

void SignalTest()
{
    /*
     * this functionis controlling the default control signals of the process and overwiting it
     */
    
    
    //the first argument is the type of signal received (integer of it)
    //the second argument is the message displaying for the signal
    //when receving the ctrl + \ argument from user.... program will give the handler's output
    signal(3, SignalHandler);
    
    for(; ;)
    {
        printf("Application Stuck cannot quit using ctrl + \\ \n");
        sleep(1);
    }
}

void SignalHandler()
{
    printf("Error\n");
}
