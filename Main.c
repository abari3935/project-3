//Abdul Bari    9140

#include <stdio.h>
#include <stdlib.h>
#include "Header.h"


int main(int argc, char* argv[])
{
    if(argc <= 1)
    {
        ArgumentsError();
    }
    //calling fileredirection funciton when receiving 1 on the index : [1]
    else if(*argv[1] == '1')
    {
        FileRedirection(argc,argv);
    }
    else if(*argv[1] == '2')
    {
        WordCount();
    }
    else if(*argv[1] == '3')
    {
        SignalTest();
    }
    else
    {
        ArgumentsError();
    }
    
}

void ArgumentsError()
{
    //error message with sample
    printf(" Incorrect number of arguments\n Sample: ./proj3 [argument a] [argument b]\n"
        " Example : \n"
        " [argument a] --> which function you want the application to perform\n"
        " 1 - for file redirection\n"
        " 2 - for pipeline processing (giving output from wordcount of ls)\n"
        " 3 - for signal handling\n"
        " [argument b] --> the arguments you want to enter\n"
        " for 1 - (any argument you enter, will redirect to file)\n"
        " for 2 - (any argument won't affect it... it does wordcount of ls)\n"
        " for 3 - (no argument required for it... \n");
}
